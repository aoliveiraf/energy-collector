import subprocess
import threading
import time
from requests import get,post
import json
import numpy as np

class AbsProcess():

   timeClose=None
   timeOpen=None
   listSample=[]
   def __init__(self, name,sampling, numSample, batchSize,listParam, urlSourceCode,urlProcess,urlSample,_type=None):
      self.name=name
      self._type=_type
      self.urlSourceCode=urlSourceCode
      self.sampling=sampling
      self.numSample=numSample
      self.urlProcess=urlProcess
      self.urlSample=urlSample
      self.listParm=listParam
      self.batchSize=batchSize
      
   def _run(self,id_process):

      for i in range(self.numSample):
         print("sample",i)

         data = self.readData()
         self.listSample.append(data)

         if self.batchSize==len(self.listSample):
            dataToSend= {
               "id_process":id_process,
               "listSample":self.listSample
            }
            post(self.urlSample, data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
            self.listSample=[]

         time.sleep(self.sampling)

   def readData(self):
      pass
      return {}

   def run(self):
      data = {
         "name":self.name,
         "_type":self._type,
         "urlSourceCode":self.urlSourceCode,
         "sampling":self.sampling,
         "numSample":self.numSample,
         "listParam":self.listParm, 
         "batchSize":self.batchSize,     
         "timeOpen": int(time.time()*1000.0)
      } 
      r= post(self.urlProcess, data = json.dumps(data),headers={"Content-Type": "application/json"})
      id_process=r.json()['_key']

      t = threading.Thread(target=self._run,args=(id_process,))
      t.start()
      t.join()


class MemoryGPUCollector(AbsProcess):
   def __init__(self, name,sampling, numSample, batchSize,listParam, urlSourceCode,urlProcess,urlSample,_type=None):
      AbsProcess.__init__(self,name,sampling,numSample,batchSize,listParam,urlSourceCode,urlProcess,urlSample,"Collector")
   
   def readData(self):

      out = "gpu_name,gpu_bus_id,power.draw\nTesla V100-SXM2-16GB, 00000000:04:00.0, 51.28 W\nTesla V100-SXM2-16GB, 00000000:05:00.0, 52.69 W\n "

      sample={"time":int(time.time()*1000.0),"csv":out}
      
      return sample

class NVIDIASMIGPUCollector(AbsProcess):
   def __init__(self, name,sampling, numSample, batchSize,listParam, urlSourceCode,urlProcess,urlSample,_type=None):
      AbsProcess.__init__(self,name,sampling,numSample,batchSize,listParam,urlSourceCode,urlProcess,urlSample,"Collector")
   
   def readData(self):
      sListParam = ','.join(map(str, self.listParm)) 
      query="--query-gpu="+sListParam
      r=subprocess.Popen(["nvidia-smi", query,"--format=csv"], stdout=subprocess.PIPE)
      out, __ = r.communicate()

      sample={"time":int(time.time()*1000.0),"csv":out.decode("utf-8") }

      return sample

collector = NVIDIASMIGPUCollector(
    name="Test",
    urlSourceCode="https://gitlab.com/aoliveiraf/energy-collector.git",
    urlSample="http://10.7.225.90:3000/sample/csv",
    urlProcess="http://10.7.225.90:3000/process",
    numSample=10,
    sampling=1,
    batchSize=2,
    listParam=["gpu_name","gpu_bus_id","power.draw"])

collector.run()